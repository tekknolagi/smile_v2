(function(app) {
    app.LogoutView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.LogoutView.prototype.template = this.template || _.template($('#logout-template').html());
        },
        events: {
            'click .login-button': 'login'
        },
        render: function() {
            console.log(app.getUser() && app.getUser().get("username"));
            var username = app.getUser() && app.getUser().get("username").capitalize();
            var logoutText = app.lang.get("logoutText", {name: username});
            this.$el.html(this.template({login: app.lang.get("login"), logoutText: logoutText}));
            logger.info("LogoutView rendered");

            return this;
        },

        remove: function() {
            logger.info("LogoutView removed");
            this.parentRemove();
        },

        login: function() {
            app.router.navigate("login", {trigger: true, replace: true});
        }
    })
})(app);
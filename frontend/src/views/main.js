(function (app) {
    app.MainView = app.View.extend({
        initialize: function (options) {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.MainView.prototype.template = this.template || _.template($('#main-template').html());
            this.refresh = true;
            return this;
        },

        render: function (options) {
            options = options || {};
            if (options.navbar) this.navbar = options.navbar;
            if (options.content) this.content = options.content;
            if (options.title) this.title = options.title;
            if (options.name) this.name = options.name;
            //reset el to template only on first load
            if (this.refresh) {
                this.$el.html(this.template());
                this.refresh = false;
                logger.info("MainView initialized");
            }

            if (options.navbar != false && options.navbar != undefined) {
                this.navbar.setElement(this.$('#app-header')).render();
            }
            if (this.navbar && this.title) {
                this.navbar.setHeader({text: this.title});
            }

            if (options.content != false && options.content != undefined) this.content.setElement(this.$('#app-content')).render();

            logger.info("MainView rendered");
            return this;
        }
    });
})(app);
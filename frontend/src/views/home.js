(function (app) {

    var firstTime = true;

    app.HomeView = app.View.extend({
        initialize: function () {
            this.title = app.lang.get("home");
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.HomeView.prototype.template = this.template || _.template($('#home-template').html());

        },

        events: {
            'touchstart .home-button': 'buttonPress',
            'touchend .home-button': 'buttonRelease',
            'click .home-button': 'buttonClick'
        },

        buttonPress: function (ev) {
            $(ev.currentTarget).addClass("touched");
        },
        buttonRelease: function (ev) {
            $(ev.currentTarget).removeClass("touched");
        },
        buttonClick: function (ev) {
            var target = ev.currentTarget.getAttribute("data-target");
            switch (target) {
                case 'trophies':
                    app.router.navigate("trophy_case", {trigger: true});
                    break;
                case 'stars':
                    break;
                case 'groups':
                    break;
                case 'answers':
                    break;
                case 'comments':
                    break;
                case 'questions':
                    break;
            }
        },

        render: function (options) {
            this.$el.html(app.loadingTemplate({msg: "Loading user stats..."}));
           // var userStats = new app.UserStats.Model();
            var that = this;
           /* userStats.fetch({
                success: function (model, response) {
                    //console.log(model);
                  //  TODO: Remove this temporary bullshit
                     // console.log("Temporarily fucking with these image URL's");
                     _.each(model.attributes.listBadges, function(element, index, list) {
                     element['image'] = element['image'].replace(":8080", "");
                     //console.log(element);
                     });*/
                    //check code
                   /* var avatar_path = app.getUser().get("photo_url"),
                    welcome_msg = app.lang.get("welcome_msg", {name: app.getUser().get('username').capitalize()});
                    var earned = _.filter(model.get("listBadges"), function(badge){ return badge.earned; });
                    console.log(earned);
                    var stats = {
                        num_trophies: earned.length,
                        num_stars: model.get("listStats")[3].data,
                        num_questions: model.get("listStats")[0].data,
                        num_answered: model.get("listStats")[6].data,
                        num_groups: 10,
                        num_comments: 10
                    }
                    that.$el.html(that.template({
                        avatar_path: avatar_path,
                        welcome_msg: welcome_msg,
                        stats: stats
                    }));

                    if (firstTime) {
                        firstTime = false;
                        app.dispatcher.trigger("alert", {type: 'success', msg: "Welcome back!"});
                    }
                    logger.info("HomeView rendered");
                },

                error: function (model, response) {

                     that.fetchError("Error fetching stats. ", response, function () {
                         this.render({});
                     });
                }
            });*/
            // added for testing
            var stats = {
                num_trophies: 5,
                num_stars: 3,
                num_questions: 4,
                num_answered: 5,
                num_groups: 10,
                num_comments: 10
            };

            that.$el.html(that.template({
                avatar_path: "",
                welcome_msg: app.lang.get("welcome_msg", {name: app.getUser().get('username').capitalize()}),
                stats: stats
            }));

            if (firstTime) {
                firstTime = false;
                app.dispatcher.trigger("alert", {type: 'success', msg: "Welcome back!"});
            }
            logger.info("HomeView rendered");
            //
            return this;
        },

        remove: function () {
            logger.info("HomeView removed");
            this.parentRemove();
        }
    })
})(app);
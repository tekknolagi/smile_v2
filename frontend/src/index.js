(function (window) {
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var app = window.app = {
        developmentMode: true,

        apiRoot: 'http://localhost/services/',

        getUser: function () {
            //var user = app.cache.get("user");
            var user = app.user;
            //if (user instanceof Backbone.Model == false) user = new app.User.Model(user);
            if (user) return user;
            if (typeof(user) === 'undefined' || user === "undefined") {
                user = undefined;
                //if user is undefined, route to login
                app.router.login();
                return user;
            }
        },
        setUser: function (user) {
           // this.cache.set('user', user);
            this.user = user;
        },

        init: function () {
            /*PRO:
             //insert dummy logger object for production only
             var logger = window.logger = {
                 error: function() { return false; },
                 info: function() { return false; },
                 debug: function() { return false; },
                 log: function() { return false; },
                 trace: function() { return false; },
                 warn: function() { return false; }
             }
             :PRO*/

            /*DEB:*/
            //initialize logger as global var, for development and debugging only
            //if debug is true, then this code will remain, overriding null logger above
            var logger = window.logger = log4javascript.getRootLogger();
            consoleAppender = new log4javascript.BrowserConsoleAppender();
            patternLayout = new log4javascript.PatternLayout("%d{HH:mm:ss} %-5p - %m{1}%n");
            consoleAppender.setLayout(patternLayout);
            logger.addAppender(consoleAppender);
            logger.setLevel(log4javascript.Level.TRACE);
            /*:DEB*/

            logger.info("app initialized");

            //disable animation for android since hardware is uncertain
            if (app.is_android) {
                $('#app-container').addClass('no-transition');
                $('#menu-container').addClass('no-transition');
                $('#menu-overlay').addClass('no-transition');
                app.no_transition = true;
            } else {
                app.no_transition = false;
            }
            //add menu animation style
            $('#menu-container').addClass(app.settings.get("menu_animation"));

            app.router = new app.Router({container: 'app-container'});
            app.dispatcher = _.clone(Backbone.Events);
            app.navbar_visible = false;
            //Shared templates
            app.loadingTemplate = _.template($('#loading-template').html());

            Backbone.history.start();
        }
    };

    //modify javascript native types
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    //user-agent detection
    var agent = navigator.userAgent.toLowerCase();
    console.log("User agent: " + agent);
    app.mobile_agent = (agent.indexOf("mobi") > -1);
    app.is_android = (agent.indexOf("android") > -1);

    /*
    //configure storage
    window.onload = function() {
        console.log("restoring from localstorage ==> cache (local memory)");
        app.cache.restoreAll();
    }
    window.onunload = function() {
        console.log("persisting from cache (local memory) ==> localstorage");
        app.cache.persistAll();
    }
    */

    /*DEV:*/
    //template loading code from:
    //  http://coenraets.org/blog/2012/01/backbone-js-lessons-learned-and-improved-sample-app/
    app.templates = {

        // Hash of preloaded templates for the app
        templates: {},

        // Recursively pre-load all the templates for the app.

        loadTemplates: function (names, callback) {

            var that = this;

            var loadTemplate = function (index) {
                var name = names[index];
                //console.log('Loading template: ' + name);
                $.get('templates/' + name + '.html', function (data) {
                    $('body').append("<script type='text/template' id='" + name + "-template'>\n" + data + "\n</script>");
                    index++;
                    if (index < names.length) {
                        loadTemplate(index);
                    } else {
                        callback();
                    }
                });
            }

            loadTemplate(0);
        },

        // Get template by name from hash of preloaded templates
        get: function (name) {
            return this.templates[name];
        }
    };

    // Run the `init` function once the DOM is ready, unless this is run from the
    // test runner.
    /*if (window.ENV !== 'test')*/
    //attach templates dynamically to end of index.html for development
    $(function () {
        app.templates.loadTemplates([
            'alert',
            'groups',
            'home',
            'loading',
            'login',
            'register',
            'main',
            'messages',
            'name',
            'navbar',
            'public',
            'menu',
            'profile',
            'settings',
            'about',
            'logout',
            'logobar',
            'trophy_case',
            'trophy_detail',
            'footer'
        ], function () {
            app.init();
        });
    });
    /*:DEV*/
    //for production, attach templates to index.html via Jakefile
    /*PRO:$(app.init);:PRO*/
}) (window);
(function (app) {

    var firstTime = true;

    app.TrophyCaseView = app.View.extend({
        initialize: function () {
            this.title = app.lang.get("trophy_case");
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.TrophyCaseView.prototype.template = this.template || _.template($('#trophy_case-template').html());
            //compile template for trophy detail only once as well and store in prototype
            if (!this.detail_template) app.TrophyCaseView.prototype.detail_template = this.detail_template || _.template($('#trophy_detail-template').html());
        },

        events: {
            "click .badge": 'badgeClick'
        },

        badgeClick: function (ev) {
            
            $('#trophy-detail').html(this.detail_template({
                trophy_name: "",
                trophy_text: "",
                user_text: ""
            }));
        },

        render: function (options) {
            this.$el.html(app.loadingTemplate({msg: "Loading user stats..."}));
            this.userStats = new app.UserStats.Model();
            var that = this;
            this.userStats.fetch({
                success: function (model, response) {
                    //
                    /*TODO: Remove this temporary bullshit
                     // 
                     _.each(model.attributes.listBadges, function(element, index, list) {
                     element['image'] = element['image'].replace(":8080", "");
                     //
                     });*/
                    //check code
                    /*TODO: use model from home, don't re-sort*/
                    var badges = _.sortBy(that.userStats.get("listBadges"), function (badge) {
                        return 1 - badge.earned;
                    });
                    that.$el.html(that.template({
                        badges: badges
                    }));

                    if (firstTime) {
                        firstTime = false;
                        app.dispatcher.trigger("alert", {type: 'success', msg: "Welcome back!"});
                    }
                    logger.info("TrophyCaseView rendered");
                },

                error: function (model, response) {
                    that.fetchError("Error fetching stats. ", response, function () {
                        this.render({});
                    });
                }
            });
            return this;
        },

        remove: function () {
            logger.info("TrophyCaseView removed");
            this.parentRemove();
        }
    })
})(app);
SMILE Global v2
============

## Dependencies
- node
- jake:
     $ npm install -g jake
- ruby
- compass:
     $ gem update --system
     $ gem install compass

## Build Instructions:
- From / repo directory, run command: jake build
- For debug build: jake build[debug]
    * The debug build includes console.log statements, log4javascript statements, and a source map for debugging in console
- Target directory is /smilem/target/

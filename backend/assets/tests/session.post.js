$.ajax({

    url: 'http://localhost:1337/session',
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    
    data: JSON.stringify({
		name: "Test Session",
		description: "A test session for testing purposes.",
		activity: "d822d9eeb077dfcba1c564df80004d99",
		group: "d822d9eeb077dfcba1c564df80003d85",
		status: "closed",
		showResults: true,
		showCorrect: true,
		hideUserDetails: false,
		enableComments: true,
		enableRatings: true,
		requireRatings: false,
		visible: false,
		timed: false,
		timerSecs: 60,
		language: "English"
	})

});
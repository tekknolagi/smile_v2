$.ajax({

    url: 'http://localhost:1337/resource/6638cf6837840fdbf0f80ec237000786/addAnswerChoices',
    type: 'PUT',
    dataType: 'json',
    contentType: 'application/json',
    
    data: JSON.stringify({
		answerChoices: [ 
		
			{
				id: 6,
				pos: 6,
				text: 'This is a test option, which has been modified.',
			},
			{
				pos: 5,
				text: 'This is a new test option.'
			}
			
		]
	})

});

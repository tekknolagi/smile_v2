/* by_id */
function(d) {
	if(d.doctype === 'response') {
		emit(d._id, d);
	}
}

/* by_resource_session */
function(d) {

	if(d.doctype === 'response') {
		emit([ d.resourceId, d.sessionId ], d);
	}

}

/* by_userId */
function(d) {
	if(d.doctype === 'response') {
		emit(d.userId, d);
	}
}

/* by_sessionId */
function(d) {
	if(d.doctype === 'response') {
		emit(d.sessionId, d);
	}
}

/* by_resourceId */
function(d) {
	if(d.doctype === 'response') {
		emit(d.resourceId, d);
	}
}

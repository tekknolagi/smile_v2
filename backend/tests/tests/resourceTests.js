//resource0 - public, activity0 (group0), user3, allowMultipleResponses, responseLimit:2
//resource1 - public, activity0, user2, allowCreatorResponse, multiSelect, selectLimit: 3
//resource2 - public, activity0, user9, allowResponseRevision

//resource3 - public, activity1 (group0 but owned by user9), user1
//resource4 - public, activity1, user1
//resource5 - public, activity1, user1

//resource6 - private, activity1, user2
//resource7 - private, activity1, user3
//resource8 - private, activity1, user4
//resource9 - private, activity1, user5

//resource10 - private, activity0, activity1, activity2, activity3, activity4, user7
//resource11 - private, activity0, activity1, activity2, activity3, activity4, user7
//resource12 - private, activity0, activity1, activity2, activity3, activity4, user7

window.resourceTests = function (callback) {

    resourceTemplate = {
        resourceType: 'flippedQuestion',
        activityIds: [ state.activity0.UUID ],
        title: "What is 3 x 5?",
        description: "",
        answerChoices: [
            {
                pos: 3,
                text: "5",
                correct: false
            },
            {
                pos: 0,
                text: "10",
                correct: false
            },
            {
                pos: 1,
                text: "15",
                correct: true,
                response: "Good job!"
            },
            {
                pos: 2,
                text: "6",
                correct: false
            }
        ], //See AnswerChoice schema below
        multiSelect: false,
        allowMultipleResponses: false,
        allowCreatorResponse: false,
        allowResponseRevision: false,
        visible: true,
        timed: false,
        timerSecs: 60,
        language: "English"
    }

    state.resource0 = _.extend({ activityIds: [state.activity0.UUID], public: true, creator: state.user3, allowMultipleResponses: true, responseLimit: 2 }, resourceTemplate);
    state.resource1 = _.extend({ activityIds: [state.activity0.UUID], public: true, creator: state.user2, allowCreatorResponse: true, multiSelect: true, selectLimit: 3 }, resourceTemplate);
    state.resource2 = _.extend({ activityIds: [state.activity0.UUID], public: true, creator: state.user9, allowResponseRevision: true }, resourceTemplate);
    state.resource3 = _.extend({ activityIds: [state.activity1.UUID], public: true, creator: state.user1 }, resourceTemplate);
    state.resource4 = _.extend({ activityIds: [state.activity1.UUID], public: true, creator: state.user1 }, resourceTemplate);
    state.resource5 = _.extend({ activityIds: [state.activity1.UUID], public: true, creator: state.user1 }, resourceTemplate);
    state.resource6 = _.extend({ activityIds: [state.activity1.UUID], public: false, creator: state.user2 }, resourceTemplate);
    state.resource7 = _.extend({ activityIds: [state.activity1.UUID], public: false, creator: state.user3 }, resourceTemplate);
    state.resource8 = _.extend({ activityIds: [state.activity1.UUID], public: false, creator: state.user4 }, resourceTemplate);
    state.resource9 = _.extend({ activityIds: [state.activity1.UUID], public: false, creator: state.user5 }, resourceTemplate);
    state.resource10 = _.extend({ activityIds: [state.activity0.UUID, state.activity1.UUID, state.activity2.UUID,
        state.activity3.UUID, state.activity4.UUID], public: false, creator: state.user7}, resourceTemplate);
    state.resource11 = _.extend({ activityIds: [state.activity0.UUID, state.activity1.UUID, state.activity2.UUID,
        state.activity3.UUID, state.activity4.UUID], public: false, creator: state.user7}, resourceTemplate);
    state.resource12 = _.extend({ activityIds: [state.activity0.UUID, state.activity1.UUID, state.activity2.UUID,
        state.activity3.UUID, state.activity4.UUID], public: false, creator: state.user7}, resourceTemplate);


    createResource0();

    function createResource0() {
        var resource = state.resource0;

        servicesTest('create resource0 - non-group member', 'resource', 'POST', state.user9,
            _.omit(resource, 'creator'), false, "success",
            function (response) {
                equal(response.error && response.error.code, 32.1, "Correct error code returned");
                createResource1();
            }
        );
    }

    function createResource1() {
        var resource = state.resource0;
        var user = resource.creator;
        servicesTest('create resource0 - group member', 'resource', 'POST', user,
            _.omit(resource, 'creator'), true, "success",
            function (response) {
                if (response && response.UUID) resource.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID returned.");
                createResource2();
            }
        );
    }

    function createResource2() {
        var resource = state.resource1;
        var user = resource.creator;
        servicesTest('create resource1 - group organizer', 'resource', 'POST', user,
            _.omit(resource, 'creator'), true, "success",
            function (response) {
                if (response && response.UUID) resource.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID returned.");
                createResource3();
            }
        );
    }

    function createResource3() {
        var resource = state.resource2;

        servicesTest('create resource0 - group organizer, different owner than self', 'resource', 'POST', state.user2,
            _.extend({ owner: state.user1.UUID }, _.omit(resource, 'creator')), false, "success",
            function (response) {
                equal(response.error && response.error.code, 32.2, "Correct error code returned");
                createResource4();
            }
        );
    }

    function createResource4() {
        var resource = state.resource2;

        servicesTest('create resource0 - group organizer, different creator than self', 'resource', 'POST', state.user2,
            _.extend({ creator: state.user1.UUID }, _.omit(resource, 'creator')), false, "success",
            function (response) {
                equal(response.error && response.error.code, 32.3, "Correct error code returned");
                createResources();
            }
        );
    }

    function createResources() {
        //create resources 1-12 in parallel
        var resources = [state.resource2, state.resource3, state.resource4, state.resource5, state.resource6, state.resource7, state.resource8, state.resource9, state.resource10, state.resource11, state.resource12];
        var callsLeft = resources.length;
        login(state.user0, function () {
            for (var i = 0, l = resources.length; i < l; i++) {
                var resource = resources[i];
                var creator = resource.creator.UUID;
                resource.creator = creator;

                $.ajax({
                    url: server + "resource",
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(resource),
                    complete: makeReturnFunc(i, resource)
                });
            }
        });

        function makeReturnFunc(i, resource) {
            return function (result, status) {
                var response = result && result.responseText && JSON.parse(result.responseText);
                if (response && response.UUID) resource.UUID = response.UUID;
                test("UUID returned, resource" + (i + 2) + " created.", function () {
                    equal(_.has(response, 'UUID'), true, result.responseText);
                });
                callsLeft--;
                if (callsLeft < 1) {
                    resourcesCreated();
                }
            }
        }

        function resourcesCreated() {
            getResource0();
        }
    }

    function getResource0() {
        var resource = state.resource0;
        servicesTest('get resource0 - public, non-group member', 'resource/' + resource.UUID, 'GET', state.user9,
            null, true, "success",
            function (response) {
                var newResource = response.resource;
                equal(newResource.UUID, resource.UUID, "Correct UUID returned");
                equal(newResource.creator, state.user3.UUID, "Correct creator UUID returned");
                equal(newResource.owner, state.user3.UUID, "Correct creator UUID returned");
                equal(newResource.answerChoices.length, resource.answerChoices.length, "Correct number of answerChoices returned");
                equal(_.isEqual(newResource.activityIds, resource.activityIds), true, "Correct activityIds returned");
                _.each(newResource.answerChoices, function (element, index, list) {
                    equal(_.has(element, "id"), true, "answerChoice " + index + " has an id.");
                })
                getResource1();
            }
        );
    }

    function getResource1() {
        var resource = state.resource6;
        servicesTest('get resource6 - private, non-group member', 'resource/' + resource.UUID, 'GET', state.user9,
            null, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                getResource2();
            }
        );
    }

    function getResource2() {
        var resource = state.resource6;
        servicesTest('get resource6 - private, admin', 'resource/' + resource.UUID, 'GET', state.user0,
            null, true, "success",
            function (response) {
                equal(response.resource && response.resource.UUID, resource.UUID, "Correct UUID returned.");
                getResource3();
            }
        );
    }

    function getResource3() {
        var resource = state.resource6;
        servicesTest('get resource6 - private, group organizer', 'resource/' + resource.UUID, 'GET', state.user2,
            null, true, "success",
            function (response) {
                equal(response.resource && response.resource.UUID, resource.UUID, "Correct UUID returned.");
                getResource4();
            }
        );
    }

    function getResource4() {
        var resource = state.resource6;
        servicesTest('get resource6 - private, group member', 'resource/' + resource.UUID, 'GET', state.user3,
            null, true, "success",
            function (response) {
                equal(response.resource && response.resource.UUID, resource.UUID, "Correct UUID returned.");
                updateResource0();
            }
        );
    }

    function updateResource0() {
        var resource = state.resource0;
        servicesTest('update resource0 - not a group member, new activity where not a member', 'resource/' + resource.UUID, 'PUT', state.user11,
            {
                name: "update name",
                resourceType: "imageGallery",
                activityIds: [state.activity4.UUID]
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                updateResource1();
            }
        );
    }

    function updateResource1() {
        var resource = state.resource0;
        servicesTest('update resource0 - group member, new activity where not a member', 'resource/' + resource.UUID, 'PUT', state.user6,
            {
                name: "update name",
                resourceType: "imageGallery",
                activityIds: [ state.activity0.UUID, state.activity4.UUID ]
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                updateResource2();
            }
        );
    }

    function updateResource2() {
        var resource = state.resource0;
        servicesTest('update resource0 - group organizer, new activity where not a member', 'resource/' + resource.UUID, 'PUT', state.user2,
            {
                name: "update name",
                resourceType: "imageGallery",
                activityIds: [ state.activity0.UUID, state.activity4.UUID ]
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 32.1, "Correct error code returned.");
                updateResource3();
            }
        );
    }

    function updateResource3() {
        var resource = state.resource0;
        servicesTest('update resource0 - activity owner, new activity where is an organizer', 'resource/' + resource.UUID, 'PUT', state.user7,
            {
                name: "update name",
                resourceType: "imageGallery",
                activityIds: [ state.activity0.UUID, state.activity4.UUID ],
                answerChoices: [
                    {
                        pos: 3,
                        text: "five",
                        correct: false
                    },
                    {
                        pos: 2,
                        text: "ten",
                        correct: false
                    },
                    {
                        pos: 1,
                        text: "six",
                        correct: false
                    },
                    {
                        pos: 0,
                        text: "six-hundred sixty-six",
                        correct: true,
                        response: "Nice Work!"
                    },
                    {
                        text: 542,
                        correct: false,
                        response: "Not close"
                    }
                ]

            }, true, "success",
            function (response) {
                equal(response.UUID, resource.UUID, "Correct UUID returned.");
                getResource5();
            }
        );
    }

    function getResource5() {
        var resource = state.resource0;
        var newAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "five",
                correct: false
            },
            {
                id: 1,
                pos: 2,
                text: "ten",
                correct: false
            },
            {
                id: 2,
                pos: 1,
                text: "six",
                correct: false
            },
            {
                id: 3,
                pos: 0,
                text: "six-hundred sixty-six",
                correct: true,
                response: "Nice Work!"
            },
            {
                id: 4,
                pos: 4,
                text: 542,
                correct: false,
                response: "Not close"
            }
        ]
        servicesTest('check resource0 - public, group member', 'resource/' + resource.UUID, 'GET', state.user4,
            null, true, "success",
            function (response) {
                equal(response.resource && response.resource.UUID, resource.UUID, "Correct UUID returned.");
                equal(_.isEqual(response.resource.answerChoices, newAnswerChoices), true, "answerChoices updated.");
                equal(_.isEqual(response.resource.activityIds, [state.activity0.UUID, state.activity4.UUID]), true, "Correct activityIds returned.");
                equal(response.resource.resourceType, "imageGallery", "Correct resourceType returned.");
                equal(response.resource.name, "update name", "Correct name returned.");
                addAnswerChoices0();
            }
        );
    }

    function addAnswerChoices0() {
        var resource = state.resource0;
        var newAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "5",
                correct: false
            },
            {
                id: 10,
                pos: 10,
                text: 11,
                correct: false
            },
            {
                text: 12,
                correct: false
            }
        ]
        servicesTest('addAnswerChoices resource0 - group member', 'resource/' + resource.UUID + "/addAnswerChoices", 'POST', state.user4,
            { answerChoices: newAnswerChoices }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                addAnswerChoices1();
            }
        );
    }

    function addAnswerChoices1() {
        var resource = state.resource0;
        var newAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "five",
                correct: false
            },
            {
                id: 10,
                pos: 10,
                text: 11,
                correct: false,
                therapy: { "massage": [4, 3, 5, 5], 'y-factor': 3.56764 }
            },
            {
                text: 12,
                correct: false
            }
        ]
        var resultAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "five",
                correct: false
            },
            {
                id: 1,
                pos: 2,
                text: "ten",
                correct: false
            },
            {
                id: 2,
                pos: 1,
                text: "six",
                correct: false
            },
            {
                id: 3,
                pos: 0,
                text: "six-hundred sixty-six",
                correct: true,
                response: "Nice Work!"
            },
            {
                id: 4,
                pos: 4,
                text: 542,
                correct: false,
                response: "Not close"
            },
            {
                id: 5,
                pos: 5,
                text: "five",
                correct: false
            },
            {
                id: 6,
                pos: 10,
                text: 11,
                correct: false,
                therapy: { "massage": [4, 3, 5, 5], 'y-factor': 3.56764 }
            },
            {
                id: 7,
                pos: 11,
                text: 12,
                correct: false
            }
        ]
        servicesTest('addAnswerChoices resource0 - group organizer', 'resource/' + resource.UUID + "/addAnswerChoices", 'POST', state.user5,
            { answerChoices: newAnswerChoices }, true, "success",
            function (response) {
                equal(_.isEqual(response.answerChoices, resultAnswerChoices), true, "Correct answerChoices returned.");
                updateAnswerChoices0();
            }
        );
    }

    function updateAnswerChoices0() {
        var resource = state.resource0;
        var updatedAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "Mexico",
                correct: false
            }
        ]
        servicesTest('updateAnswerChoices resource0 - group member', 'resource/' + resource.UUID + "/updateAnswerChoices", 'PUT', state.user8,
            { answerChoices: updatedAnswerChoices }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                updateAnswerChoices1();
            }
        );
    }

    function updateAnswerChoices1() {
        var resource = state.resource0;
        var updatedAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "Mexico",
                correct: false
            },
            {
                id: 6,
                pos: 10,
                text: 42,
                correct: true,
                therapy: { "massage": [4, 3, 5], 'z-factor': -4e10 }
            },
            {
                id: 7,
                text: 12,
                pos: 11,
                correct: false
            },
            {
                id: 11,
                text: 12,
                pos: 10,
                correct: false
            }
        ]
        var resultAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "Mexico",
                correct: false
            },
            {
                id: 1,
                pos: 2,
                text: "ten",
                correct: false
            },
            {
                id: 2,
                pos: 1,
                text: "six",
                correct: false
            },
            {
                id: 3,
                pos: 0,
                text: "six-hundred sixty-six",
                correct: true,
                response: "Nice Work!"
            },
            {
                id: 4,
                pos: 4,
                text: 542,
                correct: false,
                response: "Not close"
            },
            {
                id: 5,
                pos: 5,
                text: "five",
                correct: false
            },
            {
                id: 6,
                pos: 10,
                text: 42,
                correct: true,
                therapy: { "massage": [4, 3, 5], 'z-factor': -4e10 }
            },
            {
                correct: false,
                id: 7,
                pos: 11,
                text: 12
            }
        ]
        servicesTest('updateAnswerChoices resource0 - group organizer', 'resource/' + resource.UUID + "/updateAnswerChoices", 'PUT', state.user5,
            { answerChoices: updatedAnswerChoices }, true, "success",
            function (response) {
                equal(_.isEqual(response.answerChoices, resultAnswerChoices), true, "Correct answerChoices returned.");
                deleteAnswerChoices0();
            }
        );
    }

    function deleteAnswerChoices0() {
        var resource = state.resource0;
        var toDelete = [
            {
                id: 4
            },
            {
                id: 5
            }
        ]
        servicesTest('deleteAnswerChoices resource0 - group member', 'resource/' + resource.UUID + "/deleteAnswerChoices", 'DELETE', state.user8,
            { answerChoices: toDelete }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                deleteAnswerChoices1();
            }
        );
    }

    function deleteAnswerChoices1() {
        var resource = state.resource0;
        var toDelete = [
            {
                id: 4
            },
            {
                id: 5
            }
        ]
        var resultAnswerChoices = [
            {
                id: 0,
                pos: 3
            },
            {
                id: 1,
                pos: 2
            },
            {
                id: 2,
                pos: 1
            },
            {
                id: 3,
                pos: 0
            },
            {
                id: 6,
                pos: 10
            },
            {
                id: 7,
                pos: 11
            }
        ]
        servicesTest('deleteAnswerChoices resource0 - group organizer', 'resource/' + resource.UUID + "/deleteAnswerChoices", 'DELETE', state.user5,
            { answerChoices: toDelete }, true, "success",
            function (response) {
                equal(_.isEqual(response.answerChoices, resultAnswerChoices), true, "Correct answerChoices returned.");
                getAnswerChoices0();
            }
        );
    }

    function getAnswerChoices0() {
        var resource = state.resource7;
        servicesTest('getAnswerChoices resource7 - private, not group member', 'resource/' + resource.UUID + "/getAnswerChoices", 'GET', state.user9,
            null, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                getAnswerChoices1();
            }
        );
    }

    function getAnswerChoices1() {
        var resource = state.resource7;
        var resultAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "5",
                correct: false
            },
            {
                id: 1,
                pos: 0,
                text: "10",
                correct: false
            },
            {
                id: 2,
                pos: 1,
                text: "15",
                correct: true,
                response: "Good job!"
            },
            {
                id: 3,
                pos: 2,
                text: "6",
                correct: false
            }
        ]
        servicesTest('getAnswerChoices resource0 - private, group member', 'resource/' + resource.UUID + "/getAnswerChoices", 'GET', state.user8,
            null, true, "success",
            function (response) {
                equal(_.isEqual(response.answerChoices, resultAnswerChoices), true, "Correct answerChoices returned.");
                getAnswerChoices2();
            }
        );
    }

    function getAnswerChoices2() {
        var resource = state.resource0;
        var resultAnswerChoices = [
            {
                id: 0,
                pos: 3,
                text: "Mexico",
                correct: false
            },
            {
                id: 1,
                pos: 2,
                text: "ten",
                correct: false
            },
            {
                id: 2,
                pos: 1,
                text: "six",
                correct: false
            },
            {
                id: 3,
                pos: 0,
                text: "six-hundred sixty-six",
                correct: true,
                response: "Nice Work!"
            },
            {
                id: 6,
                pos: 10,
                text: 42,
                correct: true,
                therapy: { "massage": [4, 3, 5], 'z-factor': -4e10 }
            },
            {
                correct: false,
                id: 7,
                pos: 11,
                text: 12
            }
        ]
        servicesTest('getAnswerChoices resource0 - public, not group member', 'resource/' + resource.UUID + "/getAnswerChoices", 'GET', state.user11,
            null, true, "success",
            function (response) {
                equal(_.isEqual(response.answerChoices, resultAnswerChoices), true, "Correct answerChoices returned.");
                addAttachments0();
            }
        );
    }

    function addAttachments0() {
        var resource = state.resource0;
        servicesTest('addAttachments resource0 - public, group member', 'resource/' + resource.UUID + "/addAttachments", 'POST', state.user4,
            {
                attachments: [
                    {
                        title: "Tardigrade",
                        mime: "image/png",
                        attachment: 'base64',
                        width: '512px',
                        height: '442px',
                        crop: [0, 0, 20, 0]
                    }
                ]
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 34, "Correct error code returned.");
                addAttachments1();
            }
        );

    }

    function addAttachments1() {
        var resource = state.resource0;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");

        var img = document.createElement('img');
        img.onload = function () {
            canvas.width = img.naturalWidth;
            canvas.height = img.naturalHeight;
            ctx.drawImage(img, 0, 0);
            var base64 = canvas.toDataURL("image/png");
            base64 = base64.slice(base64.indexOf(',') + 1);
            servicesTest('addAttachments resource0 - public, group organizer', 'resource/' + resource.UUID + "/addAttachments", 'POST', state.user5,
                {
                    attachments: [
                        {
                            name: "tardigrade.jpg",
                            mime: "image/png",
                            attachment: base64,
                            width: '512px',
                            height: '442px',
                            crop: [0, 0, 20, 0]
                        }
                    ]
                }, true, "success",
                function (response) {
                    console.log(response);
                    callback && callback();
                }, true
            );
        }
        img.src = "assets/tardigrade_small.jpg";
    }
};
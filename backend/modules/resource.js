/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

    'http',
    'system.object',
    'application.modules.schema.resource'

);

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *    The validation report.
 *
 * @return mixed[]
 *    An array containing the response status, code and message.
 */
function build_validation_report(report) {

    // Build the error response
    var validators = report.error.byValidators;
    var code, message;

    if (validators.required) {

        code = 1;
        message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';

    } else if (validators.unique) {

        if (validators.unique.indexOf('name') > -1) {

            code = 16;
            message = 'Resource name already exists.';

        } else {
            code = 2;
            message = 'Unique value already exists: ["' +
                validators.unique.join('", "') + '"]';
        }

    } else {

        code = 2;
        message = 'Input validation error: ["' +
            report.error.attributes.join('", "') + '"]';

    }

    return [ 400, code, message ];
}

/**
 * Sends an error response according to the error report.
 *
 * @param object report
 *    The validation error report to build the response from.
 */
function send_validation_report(report) {

    // Send the error response
    var report = build_validation_report.call(this, report);
    this.response.sendError(report[0], report[1], report[2]);

}

/**
 * Uploads the attached files available in this request to CouchDB.
 *
 * @param object input
 *    The input data object.
 *
 * @param string id
 *    The resource document id.
 *
 * @param string revision
 *    The resource document revision.
 */
function do_attachmentsUpload(input, id, revision) {

    if (input.attachments) {
        for (var i in input.attachments) {

            var attachment = input.attachments[i];
            var file = attachment.file;

            // Handle file uploads
            if (file && 'object' === typeof file) {
                this.setAttachment(id, revision, file, attachment.token);
            }

        }
    }

}

/**
 * Clones the attachment from one document to another.
 *
 * @param object origin
 *    The document to clone to attachments from.
 *
 * @param object destination
 *    The document to clone the attachments to.
 *
 * @param string name
 *    The name of the attachment to clone from one to another.
 *
 * @param function callback
 *    A callback to handle the event completion.
 */
function do_cloneResourceAttachment(origin, destination, name, callback) {

    var _this = this;

    // Perform the attachment request and make build the next one
    var inPath = origin._id + '/' + name;
    var inOptions = this.couchDB._buildRequestOptions('GET', inPath);

    // Handle request errors
    var do_onRequestError = function () {
        $.debug('Resource: failed to clone attachment from ' + inPath);
        callback.call(_this, false);
    };

    var inRequest = $http.request(inOptions, function (response) {

        $.debug('Resource: cloning attachment from ' + inPath);

        // Build the POST request to add the new inline resource
        var outOptions = _this.couchDB._buildRequestOptions('POST', destination._id + '/' + name + '?rev=' + destination._rev);
        var outRequest = $http.request(outOptions, function () {
        });

        // Register the error handler
        outRequest.on('error', do_onRequestError);

        // Relay data from one stream to the other
        response.on('data', function (chunk) {
            $.debug('Resource: attachment clone chunk received...');
            outRequest.write(chunk);
        });

        // End the out request when the in one finished
        response.on('end', function () {
            outRequest.end();

            // This attachment has been cloned successfully
            $.debug('Resource: attachment clone complete: ' + inPath);
            callback.call(_this, true);
        });

    });

    // Register the error handler and end it
    inRequest.on('error', do_onRequestError);
    inRequest.end();

}

/**
 * Clones the attachments from one document to another.
 *
 * @param object origin
 *    The document to clone to attachments from.
 *
 * @param object destination
 *    The document to clone the attachments to.
 *
 * @param function callback
 *    A callback to handle the event completion.
 */
function do_cloneResourceAttachments(origin, destination, callback) {

    // Get the attachments to clone
    var attachments = origin._attachments;
    var attachmentCount = 0;
    var attachmentCloned = 0;
    var attachmentProcessed = 0;

    // Go through all attachments
    for (var name in origin._attachments) {

        ++attachmentCount;

        do_cloneResourceAttachment.call(this, origin, destination, name, function (success) {

            // Update the tracking variables
            ++attachmentProcessed;

            if (success) {
                ++attachmentCloned;
            }

            // Fire the given callback when all attachments are cloned.
            if (attachmentProcessed === attachmentCount) {
                callback.call(this,
                    (attachmentCloned < attachmentProcessed),
                    attachments,
                    attachmentCount,
                    attachmentCloned,
                    attachmentProcessed
                );
            }

        });

    }

}

/**
 * Creates a unique attachment token.
 *
 * @param object attachment
 *    The attachment to create the token for.
 *
 * @return string
 *    Returns the attachment token.
 */
function do_createAttachmentToken(attachment) {

    var name = attachment.name;
    var token = $.crypto.sha1('attachment' + name + attachment.id);

    // Add the extension, if any
    var index = name.lastIndexOf('.');
    if (index > -1 && index < (name.length - 1)) {
        token += name.substr(index);
    }

    return token;

}

/**
 * Validates and normalizes the attachments.
 *
 * @param object input
 *    The request input data containing the attachments.
 *
 * @param object record
 *    The existing record, if any.
 *
 * @param bool update
 *    A flag indicating wether or not this is an update context.
 *
 * @return bool
 *    Returns TRUE on success, FALSE otherwise.
 */
function do_attachmentsNormalization(input, record, update) {
    var attachments = {};
    var required = ['name', 'mime', 'attachment'];
    var errors = [];

    // Validate the attachments body
    for (var i in input.attachments) {
        var attachment = input.attachments[i];

        for (var j in required) {
            if (!attachment[required[j]]) {
                errors.push('"attachment[' + i + '][' + required[j] + ']"');
            }
        }

    }

    // Send the error report, if applicable
    if (errors.length > 0) {
        this.response.sendError(400, 1, 'Missing required fields in request: [' +
            errors.join(', ') + ']');
        return false;
    }

    // Last used ID
    var id = 0;
    var attachmentIds;

    // Determine the last used ID, if applicable
    if (record && record.attachments) {

        attachmentIds = [];

        for (var i in record.attachments) {

            // Register the attachment ids
            var attachment = record.attachments[i];
            attachmentIds.push(attachment.id);

            // Update the last used id
            if (id < attachment.id) {
                id = attachment.id;
            }

        }

    }

    // Attachments arrays
    var attachments = [];
    var _attachments = {};
    var attachmentIds = [];

    // Go through the input data
    for (var i in input.attachments) {

        var attachment = input.attachments[i];

        // Update an existing attachment
        if (update && attachment.id && record && record.attachments) {

            // Find the original attachment matching that id
            var found = false;
            var original;

            for (var j in record.attachments) {

                // Find by id
                if (record.attachments[j].id === attachment.id) {
                    original = record.attachments[j];
                    found = true;
                    break;
                }

            }

            // If IDs don't match, assign a new one
            if (!found) {
                attachment.id = ++id;
            }

        } else {
            attachment.id = ++id;
        }

        // Create the attachment token
        var token = do_createAttachmentToken.call(this, attachment);
        attachment.token = token;

        // Create the new entry for CouchDB
        if ('string' === typeof attachment.attachment) {
            _attachments[token] = {
                'content_type': attachment.mime,
                'data': attachment.attachment
            };
        }

        // Delete the binary data from the attachment and add it
        delete attachment.attachment;
        attachments.push(attachment);

        // Register the attachment id
        attachmentIds.push(id);
    }

    // Add all untouched attachments
    if (record && record.attachments && record._attachments) {

        for (var i in record.attachments) {

            var attachment = record.attachments[i];
            var token = attachment.token;

            // Make sure the attachment is valid and can be merged
            if (attachmentIds.indexOf(attachment.id) < 0 && record._attachments[token]) {

                var name = attachment.name;

                attachments.push(attachment);
                _attachments[token] = record._attachments[token];

            }

        }

    }

    // Update the applicable object.
    var object = record ? record : input;
    object.attachments = attachments;
    object._attachments = _attachments;

    return true;
}

 /**
 * Validates and normalizes the answer choices.
 *
 * @param object input
 *    The request input data containing the answer choices.
 *
 * @param object record
 *    The existing record, if any.
 *
 * @param bool update
 *    A flag indicating wether or not this is an update context.
 *
 * @return bool
 *    Returns TRUE on success, FALSE otherwise.
 */
function do_answerChoicesNormalization(input, record, update) {

    var errors = [];

    // Go through all answer choices
    for (var i in input.answerChoices) {
        var ac = input.answerChoices[i];

        if (!ac.text) {
            errors.push('"answerChoices[' + i + '][text]"');
        }
    }

    // Send the error report, if applicable
    if (errors.length > 0) {
        this.response.sendError(400, 1, 'Missing required fields in request: [' +
            errors.join(', ') + ']');

        return false;
    }

    var pos = -1, id = -1, positions = {};

    // Determine the last used ID, if applicable
    if (record && record.answerChoices) {

        for (var i in record.answerChoices) {

            var answer = record.answerChoices[i];

            if (id < answer.id) {
                id = answer.id;
            }
            if (answer.pos || answer.pos === 0) {
                positions[answer.pos] = true;
            }
            if (pos < answer.pos) {
                pos = answer.pos;
            }
        }
    }

    // New answers array
    var answers = [];
    var answerIds = [];

    for (var i in input.answerChoices) {

        // Get the answer being processed and the one being updated
        var answer = input.answerChoices[i];
        var found = false, original;

        // Assign a new ID or keep existing id, if applicable
        if (update && (answer.id || answer.id === 0) && record && record.answerChoices) {
            for (var j in record.answerChoices) {
                // Find the answer with the matching id
                if (record.answerChoices[j].id === answer.id) {
                    original = record.answerChoices[j];
                    found = true;
                    break;
                }
            }

            if (!found) {
                //if updating but has a new id, do not create
                break;
            }

            // Determine the position
            if (!answer.pos && answer.pos !== 0 && !original.pos&& !(found && original.pos === answer.pos)) {
                answer.pos = found ? original.pos : ++pos;
            }
        } else {
            answer.id = ++id;
            // Determine the position
            if (!answer.pos && answer.pos !== 0) {
                answer.pos = found ? original.pos : ++pos;
            }
        }
        if (pos < answer.pos) {
            pos = answer.pos;
        }
        //if pos is already in use, assign to next available position
        if (positions[answer.pos] && !(found && original.pos === answer.pos)) {
            answer.pos = ++pos;
        }
        positions[answer.pos] = true;

        // Register the answer
        answers.push(answer);
        answerIds.push(answer.id);

    }

    // Add all untouched answers
    if (record && record.answerChoices) {

        for (var i in record.answerChoices) {

            var answer = record.answerChoices[i];
            if (answerIds.indexOf(answer.id) < 0) {
                answers.push(answer);
            }

        }

    }

    // Update the applicable object
    var object = record ? record : input;
    object.answerChoices = _.sortBy(answers, function(answer) {
          return answer.id;
    });

    return true;

}

/**
 * Performs the resource access authorization checks.
 *
 * @param object resource
 *    The resource being checked.
 *
 * @param function callback
 *    A callback to handle the event completion.
 */
function do_resourceAccessAuthorization(resource, callback) {

    // Admins and owners don't need any additional checks
    var session = this.session.data;
    var me = session.id_user;

    // Public resources are available, as well as private for admins and owners
    if (session.admin || resource['public'] || resource.owner === me) {
        callback.call(this);
        return;
    }

    // Find all referenced activities
    var activityIds = resource.activityIds;

    this.couchDB.get('activity', 'by_id', { keys: activityIds },
        this, function (records) {

            // Array of activity owningGroup references
            var groups = [];

            for (var i in records) {

                var activity = records[i];

                // Activity owner, can safely skip validation
                if (activity.owner === me) {
                    callback.call(this);
                    return;
                }

                // Register the activity owning group
                if (activity.owningGroup && groups.indexOf(activity.owningGroup) < 0) {
                    groups.push(activity.owningGroup);
                }

            }

            // Not an activity owner, and no groups to verified.
            if (groups.length === 0) {
                send_error_34.call(this);
                return;
            }
            // Find all referenced groups
            this.couchDB.get('group', 'by_id', { keys: groups }, this, function (records) {
                for (var i in records) {

                    var group = records[i];
                    // Group owner and organizers
                    if (group.owner === me ||
                        (group.organizerIds && group.organizerIds.indexOf(me) > -1) ||
                        (group.memberIds && group.memberIds.indexOf(me) > -1)) {
                        callback.call(this);
                        return;
                    }

                }

                // Nothing gave the user permission so far..
                send_error_34.call(this);

            });

        });
}

/**
 * Performs the resource update authorization checks.
 *
 * @param object resource
 *    The resource being checked.
 *
 * @param function callback
 *    A callback to handle the event completion.
 */
function do_resourceUpdateAuthorization(resource, callback) {

    // Admins and owners don't need any additional checks
    var session = this.session.data;
    var me = session.id_user;

    // Public resources are available, as well as private for admins and owners
    if (session.admin || resource.owner === me) {
        callback.call(this);
        return;
    }

    // Does the user have right to update the activity
    var activityIds = resource.activityIds;

    this.couchDB.get('activity', 'by_id', { keys: activityIds },
        this, function (records) {

            // Array of activity owningGroup references
            var groups = [];

            for (var i in records) {

                var activity = records[i];

                // Activity owner, can safely skip validation
                if (activity.owner === me) {
                    callback.call(this);
                    return;
                }

                // Register the activity owning group
                if (activity.owningGroup && groups.indexOf(activity.owningGroup) < 0) {
                    groups.push(activity.owningGroup);
                }

            }

            // Not an activity owner, and no groups to verified.
            if (groups.length === 0) {
                send_error_34.call(this);
                return;
            }
            // Find all referenced groups
            this.couchDB.get('group', 'by_id', { keys: groups }, this, function (records) {
                for (var i in records) {

                    var group = records[i];
                    // Group owner and organizers can update an activity they do not own
                    if (group.owner === me ||
                        (group.organizerIds && group.organizerIds.indexOf(me) > -1)) {
                        callback.call(this);
                        return;
                    }

                }
                // Nothing gave the user permission so far..
                send_error_34.call(this);
            });

        });
}

function do_resourceCreateAuthorization(resource, callback) {

    // Admins don't need any additional checks
    var session = this.session.data;
    var me = session.id_user;

    if (session.admin) {
        callback.call(this);
        return;
    }

    //only admins can specify a creator
    if (resource.creator && resource.creator !== me && !session.admin) {
        this.response.sendError(403, 32.3, 'To specify a resource\'s creator you must be an admin.');
        return;
    }

    if (!resource.owner) {
        resource.owner = resource.creator || me;
    }

    // Check that owner is not different than currently authenticated user.
    if (resource.owner !== me) {
        this.response.sendError(403, 32.2, 'Admin authorization is required to set a resource\'s owner to a different user than the currently authenticated user.');
        return;
    }

    // Find all referenced activities
    var activityIds = resource.activityIds;

    this.couchDB.get('activity', 'by_id', { keys: activityIds }, this, function (records) {
        // Get the current user id
        var me = session.id_user;

        // Array of activity owningGroup references
        var groups = [];
        var success = true;

        for (var i in records) {

            var activity = records[i];

            // Activity owner, can safely skip validation
            if (activity.owner !== me) {

                success = false;
                // Register the activity owning group
                if (activity.owningGroup && groups.indexOf(activity.owningGroup) < 0) {
                    groups.push(activity.owningGroup);
                } else {
                    break;
                }

            }

        }

        // Owner of all activities
        if (success) {
            callback.call(this);
            return;
        }

        // Not an activity owner, and no groups to verified.
        if (!success && groups.length === 0) {
            send_error_32_1.call(this);
            return;
        }

        // Find all referenced groups
        this.couchDB.get('group', 'by_id', { keys: groups }, this, function (records) {
            var success = true;
            for (var i in records) {

                var group = records[i];

                // Group members can create a resource
                if (group.owner !== me && group.memberIds.indexOf(me) < 0) {

                    success = false;
                    break;

                }

            }

            if (success) {
                callback.call(this);
            } else {
                // Nothing gave the user permission so far..
                send_error_32_1.call(this);
            }

        });

    });
}

/**
 * Sanitizes the group document for display.
 *
 * @param object document
 *    The document to be sanitized.
 *
 * @return object
 *    The sanitized document.
 */
function sanitize_resource_document(document) {

    return this.sanitize(document);

};

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *    The validation report.
 *
 * @return mixed[]
 *    An array containing the response status, code and message.
 */
function build_validation_report(report) {

    // Build the error response
    var validators = report.error.byValidators;
    var code, message;

    if (validators.required) {

        code = 1;
        message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';

    } else if (validators.unique) {

        if (validators.unique.indexOf('name') > -1) {

            code = 16;
            message = 'Group name already exists.';

        } else {
            code = 2;
            message = 'Unique value already exists: ["' +
                validators.unique.join('", "') + '"]';
        }

    } else {

        code = 2;
        message = 'Input validation error: ["' +
            report.error.attributes.join('", "') + '"]';

    }

    return [ 400, code, message ];
}


function send_error_32_1() {
    this.response.sendError(403, 32.1, 'To set a resource\'s activity, you must be an admin, the resource\'s owner, or be a member in a group connected to one of a resource\'s activities.');
}
function send_error_34() {
    this.response.sendError(403, 34, 'To update a resource, you must be an admin, the resource\'s owner, or be an organizer in a group connected to one of a resource\'s activities.');
    return;
}

/**
 * Create a new resource.
 */
$.app.handle('POST', '/resource', ['smile-session', 'smile-input'], function () {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Validate the user input data
    $.schemaResource.validator.validate('POST', input,
        this, function (report, input) {

            // Report any validation errors
            if (!report.success) {
                send_validation_report.call(this, report, input);
                return;

            }

            // Performs the post to couchdb
            var do_post = function () {

                // Sanitize all given attachments
                if (input.attachments && !do_attachmentsNormalization.call(this, input)) {
                    return false;
                }

                // Sanitize all answer choices
                if (input.answerChoices && !do_answerChoicesNormalization.call(this, input)) {
                    return;
                }

                // Creator and createdOn
                var now = new Date();
                document.creator = me;
                document.createdOn = [
                    now.getFullYear(),
                    now.getMonth() + 1,
                    now.getDate(),
                    now.getHours(),
                    now.getMinutes(),
                    now.getSeconds()
                ];
                this.couchDB.store(document, this, function (success, id, revision) {
                    if (success) {
                        this.response.sendUUID(id);
                        do_attachmentsUpload(document, id, revision);
                    } else {
                        this.response.sendError(500, -1, 'Failed to post to CouchDB.');
                    }

                });

            };

            var document = {};
            $.object.extend(document, $.schemaResource.template, input);

            // Admins don't need further validation
            if (session.admin) {
                do_post.call(this);
                return;
            }
            // Validate the given activity ids
            do_resourceCreateAuthorization.call(this, document, do_post);

        });

});

/**
 * Update an existing resource.
 */
$.app.handle('PUT', '/resource/%w', ['smile-session', 'smile-input'], function (uuid) {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Validate the user input data
    $.schemaResource.validator.validate('PUT', input, this, function (report, input) {

        // Report any validation errors
        if (!report.success) {
            send_validation_report.call(this, report, input);
            return;

        }

        // Find the current record
        this.find('resource', uuid, this, function (resource) {

            do_resourceUpdateAuthorization.call(this, resource, function () {
                // Admins don't need further validation
                if (session.admin) {
                    do_post.call(this);
                    return;
                }

                //only admins can specify a creator
                if (input.creator && !session.admin) {
                    this.response.sendError(403, 32.3, 'To specify a resource\'s creator you must be an admin.');
                    return;
                }

                // Check that owner is not different than currently authenticated user.
                if (input.owner && !session.admin) {
                    this.response.sendError(403, 32.2, 'Admin authorization is required to set a resource\'s owner to a different user than the currently authenticated user.');
                    return;
                }
                //Can't set createdOn field
                input = _.omit(input, 'createdOn');

                var document = {};
                $.object.extend(document, resource, input);
                // Performs the post to couchdb
                // Sanitize all given attachments
                if (input.attachments && !do_attachmentsNormalization.call(this, input)) {
                    return false;
                }

                // Sanitize all answer choices
                if (input.answerChoices && !do_answerChoicesNormalization.call(this, input)) {
                    return;
                }

                if (input.activityIds) {
                    var validatorResource = _.clone(resource);
                    validatorResource.activityIds = _.difference(input.activityIds, resource.activityIds);
                    //don't validate for creator or owner, they are checked above with errors 32.2 and 32.3
                    validatorResource = _.omit(validatorResource, ['creator', 'owner']);
                    do_resourceCreateAuthorization.call(this, validatorResource, function() {
                        do_store.call(this, document);
                    });
                } else {
                    do_store.call(this, document);
                }
            });
            function do_store(document) {

                this.couchDB.store(document, this, function (success, ok, id, rev) {
                    if (success) {
                        this.response.sendUUID(document._id);
                        do_attachmentsUpload(document, id, rev);
                    } else {
                        this.response.sendError(500, -1, 'Failed to post to CouchDB.');
                    }
                });
            }
        });
    });
});

/**
 * Delete an existing resource.
 */
$.app.handle('DELETE', '/resource/%w', ['smile-session'], function (uuid) {

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    this.find('resource', uuid, this, function (resource) {

        // Performs the delete operation for this record
        var do_delete = function () {

            this.couchDB.delete(resource, this, function (success) {

                if (success) {
                    this.response.sendOK();
                } else {
                    this.response.sendError(500, -1, 'Failed to post to CouchDB.');
                }

            }, this);

        };

        var do_permissionError = function () {
            this.response.sendError(403, 35, 'To delete a resource, you must be an admin, the resource\'s owner, or be an organizer in a group connected to one of a resource\'s activities.');
        }

        // Admins  and owners can simply delete it
        if (session.admin || resource.owner === me) {
            do_delete.call(this);
            return;
        }

        // Make sure there are activities
        if (!resource.activityIds) {
            do_permissionError.call(this);
            return;
        }

        // Find all activities
        this.couchDB.get('activity', 'by_id', { keys: resource.activityIds },
            this, function (records) {

                var groups = [];

                // Process each activity individually
                for (var i in records) {
                    var activity = records[i];

                    // I own an activity, I can delete the session
                    if (activity.owner === me) {
                        do_delete.call(this);
                        return;
                    }

                    // Register the owning group
                    if (activity.owningGroup && groups.indexOf(activity.owningGroup) < 0) {
                        groups.push(activity.owningGroup);
                    }

                }

                // There are no groups to check.. permission denied
                if (groups.length === 0) {
                    do_permissionError.call(this);
                }

                // Get all the groups
                this.couchDB.get('group', 'by_id', { keys: groups }, this, function (groups) {

                    for (var i in groups) {

                        var group = groups[i];
                        if (group.owner === me ||
                            (group.organizerIds && group.organizerIds.indexOf(me) > -1)) {

                            do_delete.call(this);
                            return;

                        }

                    }

                    // Nothing to validate.
                    do_permissionError.call(this);

                });

            });

    });

});

/**
 * Fetch an existing resource.
 */
$.app.handle('GET', '/resource/%w', ['smile-session'], function (uuid) {

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Get the document
    this.find('resource', uuid, this, function (resource) {

        do_resourceAccessAuthorization.call(this, resource, function () {

            // Prepare the resource for display
            var result = this.sanitize(resource, this.request.query.onlyFields);

            // Insert the attachments url
            if (result.attachments) {

                var burl = this.couchDB.baseUrl + '/' + resource._id + '/';

                for (var i in result.attachments) {

                    var attachment = result.attachments[i];
                    attachment.url = burl + attachment.token;

                }

            }

            this.response.send(200, {
                success: true,
                resource: result
            });

        });

    });

});

/**
 * Post answer choices to an existing resource.
 */
$.app.handle('POST', '/resource/%w/addAnswerChoices', ['smile-session', 'smile-input'], function (uuid) {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Answer choices are required for this task
    if (!input.answerChoices) {
        this.response.sendError(400, 1, 'Missing required fields in request: ["answerChoices"]');
        return;
    }

    // Answer choices must be an array
    if (!(input.answerChoices instanceof Array)) {
        this.response.sendError(400, 2, 'Input validation error: ["answerChoices"]');
    }

    this.find('resource', uuid, this, function (resource) {

        // Make sure this user can edit this resource
        do_resourceUpdateAuthorization.call(this, resource, function () {

            // Make sure answer choices are validated properly
            if (do_answerChoicesNormalization(input, resource)) {

                // Update the existing document
                this.couchDB.store(resource, this, function (success, id, revision) {
                    if (success) {
                        this.response.send(200, {
                            success: true,
                            answerChoices: resource.answerChoices
                        });
                    } else {
                        this.response.sendError(500, -1, 'Unable to post to CouchDB.');
                    }
                });

            }

        });

    });

});

/**
 * Update answer choices to an existing resource.
 */
$.app.handle('PUT', '/resource/%w/updateAnswerChoices', ['smile-session', 'smile-input'], function (uuid) {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    this.find('resource', uuid, this, function (resource) {

        // Make sure this user can edit this resource
        do_resourceUpdateAuthorization.call(this, resource, function () {

            // Make sure answer choices are validated properly
            if (do_answerChoicesNormalization(input, resource, true)) {

                // Update the existing document
                this.couchDB.store(resource, this, function (success, id, revision) {

                    if (success) {
                        this.response.send(200, {
                            success: true,
                            answerChoices: resource.answerChoices
                        });
                    } else {
                        this.response.sendError(500, -1, 'Unable to post to CouchDB.');
                    }

                });

            }

        });

    });
});

/**
 * Remove answer choices to an existing resource.
 */
$.app.handle('DELETE', '/resource/%w/deleteAnswerChoices', ['smile-session'], function (uuid) {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Get the answerChoices array
    var answerChoices = input.answerChoices;

    if (!answerChoices) {
        this.response.sendError(400, 1, 'Missing required fields in request: ["answerChoices"]');
        return;
    }

    if (!(answerChoices instanceof Array)) {
        this.response.sendError(400, 2, 'Input validation error: ["answerChoices"]');
        return;
    }

    // Validate the given input before making the request
    var answerIds = [];
    var errors = [];

    for (var i in answerChoices) {

        var answer = answerChoices[i];

        if (!answer.id) {
            errors.push('"answerChoices[' + i + '][id]"');
            continue;
        }

        if (answerIds.indexOf(answer.id) < 0) {
            answerIds.push(answer.id);
        }

    }

    // Validation errors encountered.
    if (errors.length > 0) {
        this.response.sendError(400, 1, 'Missing required fields in request: [' + errors.join(', ') + ']');
        return;
    }

    this.find('resource', uuid, this, function (resource) {

        // Make sure this user can edit this resource
        do_resourceUpdateAuthorization.call(this, resource, function () {

            if (resource.answerChoices) {

                var answers = [];

                // Find all remaining answers
                for (var i in resource.answerChoices) {

                    if (answerIds.indexOf(resource.answerChoices[i].id) < 0) {
                        answers.push(resource.answerChoices[i]);
                    }

                }

                resource.answerChoices = answers;

                // Save changes to couchdb
                this.couchDB.store(resource, this, function (success) {

                    if (success) {
                        this.response.send(200, {
                            success: true,
                            answerChoices: _.map(resource.answerChoices, function(obj) { return _.pick(obj, 'pos','id')})
                        });
                    } else {
                        this.response.sendError(500, -1, 'Unable to post to CouchDB.');
                    }

                });

            } else {
                this.response.sendOK();
            }

        });

    });
});

/**
 * Fetches answer choices to an existing resource.
 */
$.app.handle('GET', '/resource/%w/getAnswerChoices', ['smile-session'], function (uuid) {

    // Authentication is required
    var me = this.session.data.id_user;

    this.find('resource', uuid, this, function (resource) {

        // Make sure the current user can access it
        do_resourceAccessAuthorization.call(this, resource, function () {

            // Get the answer choices array
            var answers = resource.answerChoices ?
                resource.answerChoices : [];

            this.response.send(200, {
                success: true,
                answerChoices: answers
            });

        });

    });

});

/**
 * Fetches answer choices to an existing resource.
 */
$.app.handle('POST', '/resource/%w/addAttachments', ['smile-session', 'smile-input'], function (uuid) {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Attachments are required for this task
    if (!input.attachments) {
        this.response.sendError(400, 1, 'Missing required fields in request: ["attachments"]');
        return;
    }

    // Attachments must be an array
    if (!(input.attachments instanceof Array)) {
        this.response.sendError(400, 2, 'Input validation error: ["attachments"]');
    }

    this.find('resource', uuid, this, function (resource) {

        // Make sure this user can edit this resource
        do_resourceUpdateAuthorization.call(this, resource, function () {
            // Make sure answer choices are validated properly
            if (do_attachmentsNormalization.call(this, input, resource)) {
                // Update the existing document
                this.couchDB.store(resource, this, function (success, id, revision) {
                    if (success) {
                        this.response.sendOK();
                        do_attachmentsUpload.call(this, resource, id, revision);
                    } else {
                        this.response.sendError(500, -1, 'Unable to post to CouchDB.');
                    }

                });

            }

        });

    });

});

/**
 * Updates attachments from an existing resource.
 */
$.app.handle('PUT', '/resource/%w/updateAttachments', ['smile-session', 'smile-input'], function (uuid) {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Attachments are required for this task
    if (!input.attachments) {
        this.response.sendError(400, 1, 'Missing required fields in request: ["attachments"]');
        return;
    }

    // Attachments must be an array
    if (!(input.attachments instanceof Array)) {
        this.response.sendError(400, 2, 'Input validation error: ["attachments"]');
    }

    this.find('resource', uuid, this, function (resource) {

        // Make sure this user can edit this resource
        do_resourceUpdateAuthorization.call(this, resource, function () {

            // Make sure answer choices are validated properly
            if (do_attachmentsNormalization(input, resource, true)) {

                // Update the existing document
                this.couchDB.post(resource, function (response) {

                    if (response.body.ok) {
                        this.response.sendOK();
                        do_attachmentsUpload.call(this, resource, id, revision);
                    } else {
                        this.response.sendError(500, -1, 'Unable to post to CouchDB.');
                    }

                });

            }

        });

    });

});

/**
 * Deletes attachments from an existing resource.
 */
$.app.handle('DELETE', '/resource/%w/removeAttachments', ['smile-session'], function (uuid) {

    // Input is required
    var input = this.request.body;

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Get the attachments array
    var attachments = input.attachments;

    if (!attachments) {
        this.response.sendError(400, 1, 'Missing required fields in request: ["attachments"]');
        return;
    }

    if (!(attachments instanceof Array)) {
        this.response.sendError(400, 2, 'Input validation error: ["attachments"]');
        return;
    }

    // Validate the given input before making the request
    var attachmentIds = [];
    var errors = [];

    for (var i in attachments) {

        var attachment = attachments[i];

        if (!attachment.id) {
            errors.push('"attachments[' + i + '][id]"');
            continue;
        }

        if (attachmentIds.indexOf(attachment.id) < 0) {
            attachmentIds.push(attachment.id);
        }

    }

    // Validation errors encountered.
    if (errors.length > 0) {
        this.response.sendError(400, 1, 'Missing required fields in request: [' + errors.join(', ') + ']');
        return;
    }

    $.debug('Resource: removing attachments from: ' + uuid);

    this.find('resource', uuid, this, function (resource) {

        // Make sure this user can edit this resource
        do_resourceUpdateAuthorization.call(this, resource, function () {

            if (resource.attachments) {

                var attachments = [];
                var _attachments = {};

                // Find all remaining attachments
                for (var i in resource.attachments) {

                    var attachment = resource.attachments[i];

                    if (attachmentIds.indexOf(attachment.id) < 0) {
                        attachments.push(attachment);

                        var token = attachment.token;
                        _attachments[token] = resource._attachments[token];
                    }

                }

                resource.attachments = attachments;
                resource._attachments = _attachments;

                // Save changes to couchdb
                this.couchDB.post(resource, this, function (success) {

                    if (success) {
                        this.response.sendOK();
                    } else {
                        this.response.sendError(500, -1, 'Unable to post to CouchDB.');
                    }

                }, this);

            } else {
                this.response.sendOK();
            }

        });

    });
});

/**
 * Fetches attachments from an existing resource.
 */
$.app.handle('GET', '/resource/%w/getAttachments', ['smile-session'], function (uuid) {

    // Authentication is required
    var me = this.session.data.id_user;

    this.find('resource', uuid, this, function (resource) {

        // Make sure the current user can access it
        do_resourceAccessAuthorization.call(this, resource, function () {

            // Get the answer choices array
            var attachments = resource.attachments ?
                resource.attachments : [];

            this.response.send(200, {
                success: true,
                attachments: attachments
            });

        });

    });

});

/**
 * Clones an existing resource.
 */
$.app.handle('POST', '/resource/%w/clone', ['smile-session', 'smile-input'], function (uuid) {

    // User authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Get the request input and make sure it's valid
    var input = this.request.body;

    if (!input.activityIds || !(input.activityIds instanceof Array)) {
        this.response.sendError(403, 2, 'Input validation error: ["activityIds"]');
        return;
    }

    // Find the resource being cloned
    this.find('resource', uuid, this, function (original) {

        // Create the cloned resource instance
        var clone = $.object.extend({}, original, {
            owner: (session.admin && input.owner ? input.owner : me),
            activityIds: input.activityIds
        });

        // Clean the document to prevent an update request.
        //
        // This will also clean the "_attachments" property, which will have to
        // be rebuilt once the document is created and an ID available.
        for (var property in clone) {

            if (property.indexOf('_') === 0) {
                delete clone[property];
            }

        }

        // Make sure the user can create this new resource
        do_resourceCreateAuthorization.call(this, clone, function () {

            this.couchDB.store(clone, this, function (success, id, revision) {

                if (success) {

                    do_cloneResourceAttachments.call(this, original, clone, function (success) {

                        if (success) {
                            this.response.sendOK(id);
                        } else {
                            this.response.send(200, {
                                success: true,
                                UUID: id,
                                warning: {
                                    code: -1,
                                    msg: 'Some attachments may have failed to clone.'
                                }
                            });
                        }
                    });

                } else {
                    this.response.sendError(500, -1, 'Failed to post to CouchDB.');
                }

            });

        });

    });

});


module.exports = {

    post: function () {
        this.reroute({

            '/resource/(.+)/addAnswerChoices': _post_addAnswerChoices,
            '/resource/(.+)/addAttachments': _post_addAttachments,
            '/resource/(.+)/clone': _post_clone,
            '/resource': _post

        });
    },

    put: function () {
        this.reroute({

            '/resource/(.+)/updateAnswerChoices': _put_updateAnswerChoices,
            '/resource/(.+)/updateAttachments': _put_updateAttachments,
            '/resource/(.+)': _put

        });
    },

    delete: function () {
        this.reroute({
            '/resource/(.+)/deleteAnswerChoices': _delete_deleteAnswerChoices,
            '/resource/(.+)/removeAttachments': _delete_removeAttachments,
            '/resource/(.+)': _delete

        });
    },

    get: function () {
        this.reroute({

            '/resource/(.+)/getAnswerChoices': _get_getAnswerChoices,
            '/resource/(.+)/getAttachments': _get_getAttachments,
            '/resource/(.+)': _get

        });
    }

};


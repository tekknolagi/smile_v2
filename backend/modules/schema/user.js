/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

	'system.validator'
	
);

module.exports = {

	// Module id
	_id: 'schemaUser',

	// Validator instance	
	validator: $.validator.create([
	
		// Unsafe attributes for all events
		['unsafe', 'creator,photo'],
	
		// Required attributes during POST
		['required', 'username,password,firstName,lastName,emailAddresses', 'POST'],
		
		// Either an array or undefined/null
		['string-array', 'emailAddresses', undefined, { regex: 'email' }],
		['string-array', 'roles,institutions', undefined, { regex: 'uuid' }],
		['length', 'emailAddresses', undefined, { required: false, minimum: 1 }],
		
		// Unique
		['unique', 'username', undefined, { required: false, design: 'user', view: 'by_username' }],
		['unique', 'emailAddresses', undefined, { ranged: true, required: false, design: 'user', view: 'by_emailAddresses' }],
		
		// Boolean types
		['bool', 'admin', undefined, { required: false }]
		
	
	]),
	
	// Document template
	template: {
		doctype: 'user',
	
		username: undefined,
		password: undefined,
		firstName: undefined,
		lastName: undefined,
		emailAddresses: undefined,
		creator: undefined,
		admin: false,
		country: undefined,
		region: undefined,
		city: undefined,
		roles: ["student"],
		grade: undefined,
		institutions: undefined,
		language: "English",
		hostname: "smileglobal.net",
		photoUrl: undefined
	}

};


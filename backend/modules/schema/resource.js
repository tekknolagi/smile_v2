/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

	'system.validator'
	
);

module.exports = {

	// Module id
	_id: 'schemaResource',

	// Validator instance	
	validator: $.validator.create([
	
		// Unsafe attributes for all events
		['unsafe', 'photo'],
	
		// Required attributes during POST
		['required', 'resourceType', 'POST'],
		
		// Arrays
		['array', 'answerChoices'],
		
		// Bools
		['bool', 'multiSelect,allowMultipleResponses,allowCreatorResponse,allowResponseRevision,public,visible,timed']
	
	]),
	
	// Document template
	template: {
		doctype: 'resource',
	
		name: undefined,
		resourceType: undefined,
		owner: undefined,
		creator: undefined,
		activityIds: undefined,
		attachments: undefined,
		title: undefined,
		description: undefined,
		answerChoices: undefined,
		multiSelect: false,
		selectLimit: undefined,
		allowMultipleResponses: false,
		responseLimit: undefined,
		allowCreatorResponse: false,
		allowResponseRevision: false,
		"public": true,
		visible: true,
		tags: undefined,
		gradeLevels: undefined,
		subjects: undefined,
		timed: false,
		timerSecs: 60,
		language: "English"
	}

};


/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

    'system.object',
    'application.modules.schema.activity'

);

/**
 * Sanitizes the activity document for display.
 *
 * @param object document
 *    The document to be sanitized.
 *
 * @return object
 *    The sanitized document.
 */
function sanitize_activity_document(document) {

    // Define the photo URL
    if (document._attachments && document._attachments['photo']) {
        document.photoUrl = this.couchDB.baseUrl + '/' + document._id + '/photo';
    }
    return this.sanitize(document);

};

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *    The validation report.
 *
 * @return mixed[]
 *    An array containing the response status, code and message.
 */
function build_validation_report(report) {

    // Build the error response
    var validators = report.error.byValidators;
    var code, message;

    if (validators.required) {

        code = 1;
        message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';

    } else {

        code = 2;
        message = 'Input validation error: ["' +
            report.error.attributes.join('", "') + '"]';

    }

    return [ 400, code, message ];
}

/**
 * Sends an error response according to the error report.
 *
 * @param object report
 *    The validation error report to build the response from.
 */
function send_validation_report(report) {

    // Send the error response
    var report = build_validation_report.call(this, report);
    this.response.sendError(report[0], report[1], report[2]);

}

/**
 * Create a new activity.
 */
$.app.handle('POST', '/activity/%w/clone', ['smile-session'], function (uuid) {

    // Get input and session data
    var request = this.request.body;
    //if ($.object.isEmpty(request)) request = null;

    var session = this.session.data;
    var me = session.id_user;

    var input = {
        owner: request && request.owner,
        owningGroup: request && request.owningGroup
    };
    // Only admins can set the owner property
    if (!session.admin || !input.owner) {
        input.owner = session.id_user;
    }

    // Find the existing record
    this.find('activity', uuid, this, function (record) {

        // Performs the owningGroupValidation, followed by the post
        var do_owningGroupValidation = function (input, record) {

            var do_post = function (input, record) {
                // Delete the _id and _rev, to avoid an update
                delete record._id;
                delete record._rev;

                // Create the new document
                var document = {};

                $.object.extend(document, $.schemaActivity.template, record, input);

                // Post the document
                this.couchDB.store(document, this, function (success, id, revision) {

                    if (success) {
                        this.response.sendUUID(id);
                    } else {
                        this.response.sendError(500, -1,
                            'Unable to post to CouchDB.'
                        );
                    }

                });
            };

            if (input.owningGroup && !session.admin) {

                // Find the referenced owning group
                this.couchDB.get('group', "by_id", { key: input.owningGroup }, this, function (groups) {
                    var group = groups && groups[0];

                    if (!group) {
                        this.response.send(404, {
                            success: false,
                            error: {
                                code: 13,
                                msg: 'Request owningGroup does not exist. No group found with UUID: ' + input.owningGroup
                            }
                        });

                        return;

                    }

                    if (group.owner !== me &&
                        (!group.organizerIds || group.organizerIds.indexOf(me) < 0 )) {

                        this.response.sendError(403, 24,
                            'Admin authorization is required to set owningGroup to a group in which you are not an organizer or owner.'
                        );

                        return;
                    }

                    do_post.call(this, input, record);

                });

            } else {
                do_post.call(this, input, record);
            }

        };

        // Sends the permission denied response
        var do_permissionDenied = function () {
            this.response.sendError(403, 37,
                'Insufficient permissions to clone activity.'
            );
        };

        // Without admin status, you cannot clone a private activity that is not
        // owned by you or a group in which you are a member.
        if (record['public'] || session.admin || session.id_user == record.owner) {

            do_owningGroupValidation.call(this, input, record);
            return;

        }

        // No owning group, not public, not owned by the user and not an admin
        if (!record.owningGroup) {

            do_permissionDenied.call(this);
            return;

        }

        // Get the owning group
        this.couchDB.get('group', "by_id", { key: record.owningGroup }, this, function (groups) {
            var group = groups && groups[0];

            if (!group) {
                this.response.send(404, {
                    success: false,
                    error: {
                        code: 13,
                        msg: 'Owning group of group does not exist. No group found with UUID: ' + record.owningGroup
                    }
                });

                return;
            }

            // The user is not the owner or a member
            if (group.owner !== session.id_user &&
                (!group.memberIds || group.memberIds.indexOf(session.id_user) < 0)) {
                do_permissionDenied.call(this);
            } else {
                do_owningGroupValidation.call(this, input, record);
            }

        });


    });
});

/**
 * Create a new activity.
 */
$.app.handle('POST', '/activity', ['smile-session', 'smile-input'], function () {

    // Get input and session data
    var input = this.request.body;
    var session = this.session.data;
    var me = session.id_user;

    // Only admins can set the owner property
    if (!input.owner) {
        input.owner = session.id_user;
    } else if (input.owner != session.id_user && !session.admin) {
        //if non-admin and trying to create an activity with a different owner
        this.response.sendError(403, 23,
            'Admin authorization is required to set the owner of an activity to a user other than yourself.'
        );
        return;
    }


    // Validate the user input data
    $.schemaActivity.validator.validate('POST', input,
        this, function (report, input) {

            // Report any validation errors
            if (!report.success) {
                send_validation_report.call(this, report, document);
                return;

            }

            // Performs the post operation after group validation
            var _doPost = function (input) {

                var document = $.object.extend({}, $.schemaActivity.template, input);

                this.couchDB.store(document, this, function (success, id, revision) {

                    if (success) {

                        this.response.sendUUID(id);

                        // Add the group photo as an attachment of this document
                        var photo = this.request.files['photo'];

                        if (photo) {
                            this.setAttachment(id, revision, photo, 'photo');
                        }

                    } else {

                        $.error('ActivityModule: failed to store document in couchdb.');
                        this.response.sendError(500, -1, 'Internal server error.');

                    }

                });

            };


            if (input.owningGroup && !session.admin) {

                // Find the referenced owning group
                this.couchDB.get('group', "by_id", { key: input.owningGroup }, this, function (records) {

                    var record = records[0];
                    if (!record) {
                        this.response.send(404, {
                            success: false,
                            error: {
                                code: 13,
                                msg: 'No group found with UUID: ' + input.owningGroup
                            }
                        });

                        return;
                    }

                    if (record.owner !== me &&
                        (!record.organizerIds || record.organizerIds.indexOf(me) < 0 )) {
                        this.response.sendError(403, 24,
                            'Admin authorization is required to set owningGroup to a group in which you are not an organizer or owner.'
                        );

                        return;
                    }

                    _doPost.call(this, input);

                });

            } else {
                _doPost.call(this, input);
            }

        });

})
;

/**
 * Updates an existing activity.
 */
$.app.handle('PUT', '/activity/%w', ['smile-session', 'smile-input'], function (uuid) {

    // Make sure input has been given
    var input = this.request.body;
    var session = this.session.data;
    var me = session.id_user;

    // Only admins can set the owner property
    if (!session.admin && input.owner && input.owner != session.id_user) {
        //if non-admin and trying to update activity with a different owner
        this.response.sendError(403, 23,
            'Admin authorization is required to modify the owner of an activity that you do not own.'
        );

        return;
    }

    // Validate the user input data
    $.schemaActivity.validator.validate('PUT', input,
        this, function (report, input) {

            // Report any validation errors
            if (!report.success) {
                send_validation_report.call(this, report, input);
                return;

            }

            // Performs the post operation after group validation
            var _doPost = function (uuid, input) {
                // Find the current record
                this.find('activity', uuid, this, function (record) {

                    // Update the existing document
                    var document = $.object.extend({}, $.schemaActivity.template,
                        record, input
                    );

                    this.couchDB.store(document, this, function (success, id, revision) {

                        if (success) {

                            this.response.sendOK();

                            // Add the group photo as an attachment of this document
                            var photo = this.request.files['photo'];

                            if (photo) {
                                this.setAttachment(id, revision, photo, 'photo');
                            }

                        } else {

                            $.error('ActivityModule: failed to store document in couchdb.');
                            this.response.sendError(500, -1, 'Internal server error.');

                        }

                    });

                });

            };

            if (input.owningGroup && !session.admin) {
                // Find the referenced owning group
                this.couchDB.get('group', "by_id", { key: input.owningGroup }, this, function (records) {
                    var record = records[0];
                    if (!record) {
                        this.response.send(404, {
                            success: false,
                            error: {
                                code: 13,
                                msg: 'No group found with UUID: ' + input.owningGroup
                            }
                        });

                        return;
                    }

                    if (record.owner !== me &&
                        (!record.organizerIds || record.organizerIds.indexOf(me) < 0 )) {

                        this.response.sendError(403, 24,
                            'Admin authorization is required to set owningGroup to a group in which you are not an organizer or owner.'
                        );

                        return;
                    }

                    _doPost.call(this, uuid, input);

                });

            } else {
                _doPost.call(this, uuid, input);
            }

        });

});

/**
 * Deletes an existing activity.
 */
$.app.handle('DELETE', '/activity/%w', ['smile-session'], function (uuid) {

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Find the current record
    this.find('activity', uuid, this, function (record) {

        // Admin authorization required to delete an activity owned by a
        // different user.
        if (!session.admin && session.id_user !== record.owner) {
            this.response.sendError(403, 7,
                'Admin authorization required to call this function.'
            );

            return;
        }

        // Delete the document from CouchDB
        this.couchDB.delete(record, this, function (success) {

            if (success) {
                this.response.sendOK();
            } else {
                this.response.sendError(500, -1, 'Failed to delete from CouchDB.');
            }

        });

    });
});

/**
 * Fetches existing activities.
 */
$.app.handle('GET', '/activity/groupActivities/%w', ['smile-session'], function (uuid) {

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    //The user must be authenticated as the owner or organizer of a group to view its group activities
    // Find the referenced owning group
    this.couchDB.get('group', "by_id", { key: uuid }, this, function (records) {
        var record = records[0];
        if (!record) {
            this.response.send(404, {
                success: false,
                error: {
                    code: 13,
                    msg: 'No group found with UUID: ' + uuid
                }
            });
            return;
        }
        if (record.owner !== me &&
            (!record.organizerIds || record.organizerIds.indexOf(me) < 0 ) && !session.admin) {

            this.response.sendError(403, 25,
                'Admin authorization is required to view the group activities from a group in which you are not an owner nor an organizer.'
            );

            return;
        }
        //find matching activities for group
        this.couchDB.get('activity', 'by_owningGroup', { key: uuid }, this, function (records) {

            var results = [];

            for (var i in records) {
                results.push(sanitize_activity_document.call(this, records[i]));
            }

            this.response.send(200, {
                success: true,
                activities: results
            });

        });
    });
});

/**
 * Fetches existing activities.
 */
$.app.handle('GET', '/activity/userActivities/%w', ['smile-session'], function (uuid) {

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    //only admins can request a different users activity
    if (!session.admin && uuid !== me) {
        this.response.sendError(403, 26,
            'Admin authorization is required to view the user activities for a user other than the currently authenticated user.'
        );

        return;
    }

    this.couchDB.get('activity', 'by_owner', { key: uuid }, this, function (records) {

        var results = [];

        for (var i in records) {
            results.push(sanitize_activity_document.call(this, records[i]));
        }

        this.response.send(200, {
            success: true,
            activities: results
        });

    });

});

/**
 * Fetches an existing activity.
 */
$.app.handle('GET', '/activity/%w', ['smile-session'], function (uuid) {

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Find the current record
    this.find('activity', uuid, this, function (record) {

        // Return public activities, as well as those owned by the
        // currently authenticated user
        if (record['public'] || session.admin ||
            record.owner === session.id_user) {

            this.response.send(200, {
                success: true,
                activity: sanitize_activity_document.call(this, record)
            });

            return;
        }

        // Check whether or not the user belongs to the owning group
        if (record.owningGroup) {

            // Find the referenced owning group
            this.couchDB.get('group', 'by_id', { key: record.owningGroup }, this, function (groups) {
                var group;
                if (groups.length > 0) {
                    group = groups[0];
                } else {
                    //Return error if owningGroup does not exist and you are not admin or owner of activity
                    this.response.send(404, {
                        success: false,
                        error: {
                            code: 13,
                            msg: 'Insufficient permissions to view activity. ' +
                                'No group found with owningGroup\'s UUID: ' + record.owningGroup
                        }
                    });
                    return;
                }
                if (group.owner !== session.id_user &&
                    (!group.memberIds || group.memberIds.indexOf(session.id_user) < 0)) {

                    this.response.sendError(403, 24,
                        'Insufficient permissions to view activity.'
                    );

                    return;

                }

                // Send the activity
                this.response.send(200, {
                    success: true,
                    activity: sanitize_activity_document.call(this, record)
                });

            });

        }

    });

});
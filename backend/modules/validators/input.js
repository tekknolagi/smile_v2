/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

/**
 * Requires input in the request body, returning a generic error message if
 * such does not happen.
 *
 * @param HttpMessage request
 *	The instance of the request being validated.
 *
 * @param HttpResponse response
 *	The response associated with the request.
 *
 * @param HttpSession session
 *	The session associated with the request.
 */
$.app.setCustomValidator('smile-input', function(request, response, session) {

	if($.object.isEmpty(request.body)) {
		response.sendError(400, 1, 'Required input is missing.');
		return false;
	}
	
	return true;

});
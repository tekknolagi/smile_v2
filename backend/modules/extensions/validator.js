/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

	'system.validator'

);

/**
 * Makes sure the given attribute does not exist in any other document of the
 * same doctype.
 *
 * @param object document
 *	The document being validated.
 *
 * @param string attribute
 *	The attribute being validated.
 *
 * @param mixed value
 *	The value being validated.
 *
 * @param object|undefined options
 *	An object containing additional options for this validator.
 *
 * @param callback(success) callback
 *	The validation result reporting callback.
 */
$.validator.prototype._coreValidators.unique = function(document, attribute, value, options, callback) {

	// Build the local options object.
	var options = $.object.extend({
		required: false
	}, options);

	// Attribute is not required
	if(!value) {
		callback.call(this, !options.required);
		return;
	}

	// Get the document type name
	var design = options.design ? options.design : document.doctype;
	var view = options.view ? options.view : ('by_' + attribute);
	
	if(!design || !view) {
		throw 'Invalid CouchDB design document or view: ' + design + '/' + view + ';';
	}
	
	// Build the query object dynamically
	var query = {};
	var property = (value instanceof Array && options.ranged) ? 'keys' : 'key';
	query[property] = value;
	
	// Find the document in the couch database	
	$.app.couchDB.get(design, view, query, this, function(records) {
	
		$.debug(
			'ValidatorUnique: document._id="' + document._id + 
			'"; record._id="' + (records[0] ? records[0]._id : 'undefined') + '"'
		);
	
		// If the only document with this value is this one, it's still unique
		var success = (!records[0] || records[0]._id === document._id);
		callback.call(this, success);
	
	});
	

};

/**
 * Makes sure the value is an array of strings.
 *
 * @param object document
 *	The document being validated.
 *
 * @param string attribute
 *	The attribute being validated.
 *
 * @param mixed value
 *	The value being validated.
 *
 * @param object|undefined options
 *	An object containing additional options for this validator.
 *
 * @param callback(success) callback
 *	The validation result reporting callback.
 */
$.validator.prototype._coreValidators['string-array'] = function(document, attribute, value, options, callback) {

	// Build the local options object.
	var options = $.object.extend({
		required: false,
		nullable: false,
		regex: undefined
	}, options);

	// A value was not given
	if(!value) {
		callback.call(this, (!options.nullable && value === null ? false : !options.required));		
		return;
		
	}
	
	// Not an array
	if(!(value instanceof Array)) {
		callback.call(this, false);
		return;
	}
	
	// Get the regex to test the strings with
	var regex = options.regex;
	
	// Email regex
	if(regex === 'email') {
		regex = /^.*\@.*$/;
	}
	
	// Word/uuid
	if(regex === 'uuid' || regex === 'word') {
		regex = /^\w+$/;
	}
	
	var success = true;
	
	// Make sure all values are strings
	for(var i in value) {
		
		var item = value[i];
		
		// Make sure it's a string matching a possible regex
		if('string' !== typeof item || (regex && !regex.test(item))) {
			success = false;
			break;
		}
		
	}
	
	callback.call(this, success);
	
};

/**
 * Makes sure the value is an array of ints.
 *
 * @param object document
 *	The document being validated.
 *
 * @param string attribute
 *	The attribute being validated.
 *
 * @param mixed value
 *	The value being validated.
 *
 * @param object|undefined options
 *	An object containing additional options for this validator.
 *
 * @param callback(success) callback
 *	The validation result reporting callback.
 */
$.validator.prototype._coreValidators['number-array'] = function(document, attribute, value, options, callback) {

	// Build the local options object.
	var options = $.object.extend({
		required: false,
		convert: true,
		integer: true
	}, options);

	// A value was not given
	if(!value) {
		callback.call(this, !options.required);
		return;
	}
	
	var success = true;
	
	// Make sure all values are strings
	for(var i in value) {
		
		var item = value[i];
		
		// Make sure it's a string matching a possible regex
		if($.object.getSpecificType(item) !== 'int') {
		
			// Convert a string to an integer
			if(options.convert) {
				if(!isNaN(item)) {
					value[i] = options.integer ? parseInt(item) : parseFloat(item);
					continue;
				}
			}
			
			success = false;
			break;
		}
		
	}
	
	callback.call(this, success);
	
};

